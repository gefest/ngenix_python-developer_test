import uuid
import random
import string


def get_random_string(char_count: int):
    return ''.join(
        random.choice(string.ascii_lowercase + string.ascii_uppercase + string.digits) for _ in range(char_count)
    )


def create_object(text):
    return f"<object name='{text}'/>"


def xml_objects(funk, count):
    template = "<objects>{objects}</objects>"
    return template.format(objects=''.join(funk(get_random_string(random.randint(10, 100))) for _ in range(count)))


def create_xml_strict(objects_funk=xml_objects,
                      object_funk=create_object,
                      object_count=None) -> str:
    if not object_count:
        object_count = random.randint(1, 10)
    return f"<root><var name='id' value='{uuid.uuid4()}'/>" \
           f"<var name='level' value='{random.randint(1, 100)}'/>" \
           f"{objects_funk(object_funk, object_count)}" \
           f"</root>"
