from pathlib import Path

STORAGE = Path(__file__).parent.joinpath('storage')
try:
    STORAGE.mkdir()
except FileExistsError:
    pass
CSV_ONE = STORAGE.joinpath('csv_result_one.csv')
CSV_TWO = STORAGE.joinpath('csv_result_two.csv')

CSV_ONE_FIELDS = ['id', 'level']
CSV_TWO_FIELDS = ['id', 'object_name']
STOP_PROC = 'STOP'