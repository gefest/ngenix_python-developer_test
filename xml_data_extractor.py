from pathlib import Path
from multiprocessing import Process, Queue
import zipfile
import xml.dom.minidom
import csv
from config import (
    CSV_ONE,
    CSV_TWO,
    CSV_ONE_FIELDS,
    CSV_TWO_FIELDS,
    STORAGE,
    STOP_PROC
)


def unzip(zip_path):
    with zipfile.ZipFile(zip_path, 'r') as zfile:
        zfile.extractall(STORAGE)
        return [ex_file.filename for ex_file in zfile.filelist]


def xml_data_extract(xml_path: Path):
    xml_dom = xml.dom.minidom.parse(xml_path.open('r'))
    xml_data = {}
    for xml_var in xml_dom.getElementsByTagName('var'):
        xml_data[xml_var.getAttribute('name')] = xml_var.getAttribute('value')
    xml_data['object_names'] = [node.getAttribute('name') for node in xml_dom.getElementsByTagName('object')]
    return xml_data


def create_csv_files():
    with open(CSV_ONE, 'w') as file:
        csv_file = csv.DictWriter(file, CSV_ONE_FIELDS, delimiter=';')
        csv_file.writeheader()

    with open(CSV_TWO, 'w') as file:
        csv_file = csv.DictWriter(file, CSV_TWO_FIELDS, delimiter=';')
        csv_file.writeheader()


def csv_data_write(queue):
    while True:
        with open(CSV_TWO, 'a') as file_one, open(CSV_ONE, 'a') as file_two:
            data = queue.get()
            if data == STOP_PROC:
                break
            csv_file_one = csv.DictWriter(file_one, CSV_TWO_FIELDS, delimiter=';')
            csv_file_two = csv.DictWriter(file_two, CSV_ONE_FIELDS, delimiter=';')
            csv_file_one.writerows(
                {'id': data['id'], 'object_name': object_name} for object_name in data['object_names'])
            data.pop('object_names')
            csv_file_two.writerow(data)


def extract_files(zpath, queue):
    for xml_name in unzip(zpath):
        xml_path = STORAGE.joinpath(xml_name)
        data = xml_data_extract(xml_path)
        queue.put(data)
        xml_path.unlink()


if __name__ == '__main__':
    create_csv_files()
    q = Queue()
    writer = Process(target=csv_data_write, args=(q,))
    prs = []
    for z_path in STORAGE.glob('*.zip'):
        proc = Process(target=extract_files, args=(z_path, q))
        proc.start()
        prs.append(proc)
    writer.start()

    # тут я перемудрил, давно не работал с мультипроцем
    # Идея была в том что-бы убедиться в завершении всех процессов
    # и послать в Queue команду остановки, иначе будет блокировка

    while prs:
        proc = prs.pop(0)
        if proc.exitcode == 0:
            proc.close()
            continue
        prs.append(proc)

    else:
        q.put(STOP_PROC)

    while not writer.exitcode == 0:
        continue
    else:
        q.close()
