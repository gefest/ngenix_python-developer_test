from pathlib import Path


def create_xml_file(file_path: Path, data: str):
    file_path.write_text(data)
