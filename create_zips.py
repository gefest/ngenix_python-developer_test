import uuid
from pathlib import Path
import zipfile

from multiprocessing import Process

from create_xml_files import create_xml_file
from xml_data_creator import create_xml_strict
from config import STORAGE


def create_zip_file(path: Path, xml_files_paths: tuple, delete_files: bool = True):
    with zipfile.ZipFile(path, 'w') as zfile:
        for xml_file in xml_files_paths:
            zfile.write(xml_file, xml_file.name, compress_type=zipfile.ZIP_DEFLATED)
            if delete_files:
                xml_file.unlink()


def main():
    xml_files_paths = tuple(STORAGE.joinpath(f"{uuid.uuid4()}.xml") for _ in range(100))
    for xml_path in xml_files_paths:
        create_xml_file(xml_path, create_xml_strict())
    create_zip_file(STORAGE.joinpath(f"{uuid.uuid4()}.zip"), xml_files_paths)


if __name__ == '__main__':
    for _ in range(50):
        proc = Process(target=main)
        proc.start()
